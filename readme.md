![image](markdownstuff/images/titlebanner.png)
# WorldDownloader
EnderRails WorldDownloader is designed specifically for **ENDERRAILS** servers

It allows you to download a world from a URL instead of asking staff to upload your world.
The plugin is preinstalled into all of our servers for your benefit
#
No documentation for compiling this plugin will be provided due to the obvious reasons
#
Commands:  
**`<optional> - (required)`**  
  
`/worlddl dl (world name) (url)`  - Downloads a world from the URL (Requires worlddl.dl) **HAS TO BE A ZIP**  
  
Permissions:  
  
**`worlddl.dl` - Allows downloading of a world**  