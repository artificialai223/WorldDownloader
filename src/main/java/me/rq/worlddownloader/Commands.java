package me.rq.worlddownloader;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class Commands implements CommandExecutor {
    private final WorldDownloader plugin;

    public Commands(WorldDownloader plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("worlddownload")) {
            if (args.length == 2) {
                try {
                    String name = args[0];
                    String url = args[1];
                    String file = new File(WorldDownloader.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent() + "\\" + name + ".zip";
                    Converter.urlToFile(new URL(url), new File(file), name, sender);
                } catch (IOException | URISyntaxException e) {
                    e.printStackTrace();
                }
            } else {
                sender.sendMessage(ChatColor.RED + "/worlddl <name> <url>");
            }
            return true;
        }
        return false;
    }
}