package me.rq.worlddownloader;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class Zipper {
    public static void unzip(String file, String destination) {
        try {
            new ZipFile(file).extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }
}
