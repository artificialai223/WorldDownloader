package me.rq.worlddownloader;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;

public class Converter {
    public static void urlToFile(URL url, File file, String label, CommandSender sender) throws IOException {
        try {
            InputStream input = url.openStream();
            if(file.exists()) {
                if(file.isDirectory())
                    sender.sendMessage(ChatColor.RED + "That world already exists.");

                if(!file.canWrite())
                    sender.sendMessage("World '" + file + "' couldn't be written.");
            } else {
                File parent = file.getParentFile();
                if((parent != null) && (!parent.exists()) && (!parent.mkdirs())) {
                    sender.sendMessage("That world couldn't be created.");
                }
            }
            FileOutputStream output = new FileOutputStream(file);
            byte[] buffer = new byte[4096];
            int n = 0;
            while(-1 != (n = input.read(buffer))) {
                output.write(buffer, 0, n);
            }
            input.close();
            output.close();
            Zipper.unzip(file.getPath(), file.getParentFile().getParent());
            Files.delete(file.toPath());
            sender.sendMessage(ChatColor.GREEN + "World downloaded successfully!");
        } catch (IOException ioEx) {
            sender.sendMessage(ChatColor.RED + "Something went wrong when downloading the world.");
        }
    }
}
