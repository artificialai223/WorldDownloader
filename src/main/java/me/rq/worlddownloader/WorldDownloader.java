package me.rq.worlddownloader;

import org.bukkit.plugin.java.JavaPlugin;

public final class WorldDownloader extends JavaPlugin {

    @Override
    public void onEnable() {
        // Plugin startup logic
        getCommand("worlddownload").setExecutor(new Commands(this));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
